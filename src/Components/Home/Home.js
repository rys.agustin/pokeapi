// Home page donde voy a mostrar los distintos tipos de contenidos

import React, {useState} from "react";
import MainHeader from "../Mains/MainHeader";
import {fetchPokemon} from "../Api/fetchPokemon";
import PokemonsContainer from "../Mains/PokemonsContainer"; 
import Button from '../UI/Button';
import Input from '../UI/Input';
import Pokecard from "../UI/Pokecard";
import Card from "../UI/Card";

import styles from './Home.module.css'

// mains colors  French blue #2A75BB      Sunglow  #FFCB05   Dark #071E22     RED #EE2E31    REDISH #F3B3A6    CELESTE #a6d8f3

const Home = () => {
    const [userInput, setUserInput] = useState('ditto')
    const [pokemon, setPokemon] = useState()
    const [pokemonsList, setPokemonsList] = useState([])

    const inputUserHandler = (event) => {
        setUserInput(event.target.value.toLowerCase())
    }
    
    const searchPokemonHandler = async () => {
        const pokemon = await fetchPokemon(userInput)      
        setPokemon(pokemon)
        console.log(pokemon.sprites)
        return pokemon
    }

    const pickedPokemosnHandler = (pickedPoke) => {
        console.log('Hi from Home')
        console.log(pickedPoke.type)
        
        setPokemonsList((prevPokemons) => {
            return [...prevPokemons, pickedPoke]
        })    
    }


    return (
        <>
            <MainHeader/>
            <div className={styles.find}>
                <Input type={"text"} placeholder={'Search Pokemon'} onChange={inputUserHandler}></Input>
                <Button  type="button" onClick={searchPokemonHandler} content="Catch it!">  </Button>
            </div>
            <div className={styles.containers}>
                {pokemon &&  <Pokecard pokemonName={pokemon.name} abilities={pokemon.abilities} types={pokemon.types} type={pokemon.types[0].type.name} weight={pokemon.weight}  images={{front: pokemon.sprites.front_default, back: pokemon.sprites.back_default}} onPick={pickedPokemosnHandler}/>} 
                {pokemonsList && <Card pokemonPicked = {pokemonsList} />}
            </div>
            <PokemonsContainer/>
        </>
    );
}

export default Home;


























// const [pickedPokemon, setPickedPokemon] = useState([])
    
// const pickedPokemonHandler = (image, name) => {
//     // console.log(pickedPokemon)

//     setPickedPokemon((prevPokeList)=>{
//         return [...prevPokeList, {name: name, img: image}]
//     })



// }

// {pickedPokemon && <Card pokemons={pickedPokemon}/>}
//   {pokemon &&  <Pokecard pokemonName={pokemon.name} abilities={pokemon.abilities} types={pokemon.types} type={pokemon.types[0].type.name} weight={pokemon.weight}  image={pokemon.sprites.front_default} onPick={pickedPokemonHandler}/>} 