import React from 'react';

import styles from './Card.module.css';

const Card = (props) => {
  
    return (
        <section className={styles.section}>
             <ul className={styles.picked}>
                {props.pokemonPicked.map((pokemon) => {
                    return <li key={pokemon.name} className={ styles.card + ' ' + styles[`${pokemon.type}`]}> 
                       <div className={styles.name}> {pokemon.name} </div>
                       <img src={pokemon.image} alt={pokemon.name}></img>
                    </li>
                })}
    
            </ul>
        </section>

    ) ;

}

export default Card