// componente con texto que va a ir en el header con los distintos tipos de "menus" por Tipos / Todos los pokemones / Pokemon individual / bayas / etc
import React from "react";
import styles from "./Navigation.module.css";

const Navigation = (props) => {
    return (
        <a href="/" className={styles.navigation}> {props.text}</a>
    )
}

export default Navigation;