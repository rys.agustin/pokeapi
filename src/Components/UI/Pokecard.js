// contenedor para cada pokemon
import React, {useState} from 'react'
import types from './PokemonsTypes.module.css'
import styles from './Pokecard.module.css'
import leftArrow from '../../Assets/images/l-arrow.png'
import rightArrow from '../../Assets/images/r-arrow.png'

const Pokecard = (props) =>  {
    const toUpperCase = str => str.charAt(0).toUpperCase() + str.slice(1);
    const [pokemonImage, setPokemonImage] = useState(props.images.front)

    const clickHandler = () => { // lifting the state up!
        const pickedPokemon = {
            id: props.pokemonName, 
            name: props.pokemonName, 
            image: props.images,
            type: props.type
        }
        props.onPick(pickedPokemon)
    }


    const rotateHandler = () => {
        pokemonImage === props.images.front ? setPokemonImage(props.images.back) : setPokemonImage(props.images.front);
    }

    return (
    <section className={styles.pokecard + ' ' + types[`${props.type}`]}>
        <div className={styles.description}>
            <div className={styles.description__imgs}>
                    <button onClick={rotateHandler}> <img alt='left-arrow' src={leftArrow}></img> </button>
                    <img className={styles.description__img} src={pokemonImage} alt={props.pokemonName}></img>
                    <button onClick={rotateHandler}> <img alt='right-arrow'  src={rightArrow}></img> </button>
            </div>
            <h3 className={styles.description__title}>{toUpperCase(props.pokemonName)}</h3>
        </div>
        <div className={styles.abilities}>
            <h4 className={styles.abilities__title}>Abilities</h4>
            <ul className={styles.abilties__list}> 
                {props.abilities.map(ability => <li  className={styles.abilties__item} key={ability.ability.name}>{toUpperCase(ability.ability.name)}</li>)}
            </ul>
        </div>
        <div className={styles.types}>
            <h4 className={styles.types__title}>Types</h4>
            <ul className={styles.types__list}>
                {props.types.map(type => <li key={type.type.name}>{toUpperCase(type.type.name)}</li>)}
            </ul>
        </div>
        <div>
        </div>
        <div className={styles.weight}>
            <h4 className={styles.weight__title}>Weight</h4>
            <p  className={styles.weight__item}>{props.weight/10} Kg</p>
        </div>
        <div className={styles.pokeball} onClick={clickHandler}>
            <div className={styles.pokeball__btn}></div>
        </div>
    </section>
   )
}

export default Pokecard;
