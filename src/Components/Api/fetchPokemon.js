// componente al que le va a pegar a la api, podría ser un sólo componente que reciba el distinto tipo de fetch

// const fetchPokemon = async (pokemon, fetchType)
export const fetchPokemon = async (pokemon) => {
    try {
        let url = `https://pokeapi.co/api/v2/pokemon/${pokemon}`
        const res = await fetch(url);
        const data = await res.json();
        console.log(data)
        return data;
    } catch (error) {

    }
} //  default fetchPokemon;


export const getPokedex = async (limit=20 , offset=0) => {
    try {
        const url =  `https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`;
        const res = await fetch(url);
        const data = await res.json();
        return data;
    } catch (error) {
        console.error(error)
    }
} // export default fetchPokemons;


export const getPokemonData = async (url) => {
    try {
        const res = await fetch(url);
        const data = res.json();
        return data;
    } catch (error) {
        console.log(error)
    }

}
