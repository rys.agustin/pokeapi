//rfce
import React, {useState, useEffect} from "react";
import {getPokedex, getPokemonData} from "../Api/fetchPokemon";

import style from './PokemonsContainer.module.css'
import types from '../UI/PokemonsTypes.module.css'


const PokemonsContainer = () => {
    const toUpperCase = str => str.charAt(0).toUpperCase() + str.slice(1);
    const [pokedex, setPokedex] = useState([])
    const [showMore, setShowMore ] = useState(14)

    const fetchPokemons = async (quantity) => {
        try {
            // data contains the pokemons, and the key "results" contains the pokemon's name
            const data = await getPokedex(quantity); 
            const promises = data.results.map( async (pokemon) => {
                return await getPokemonData(pokemon.url)
            })
            const results = await Promise.all(promises)
            setPokedex(results)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        fetchPokemons(showMore)
    }, [showMore])

    const getMorePokemons = () => {
        setShowMore(showMore + 7)
    }

console.log(pokedex)
    return(
        <section className={style.main}>
            <ul className={style['pokedex-container']}>
                {pokedex.map((pokemon, idx) => {
                    return <li key={idx} className={types[`${pokemon.types[0].type.name}`]}> 
                    <img src={pokemon.sprites.front_default} alt={pokemon.name}></img>
                    <div key={pokemon.name}>
                        #{idx+1}: {toUpperCase(pokemon.name)}
                    </div>
                </li>})}
            </ul>
            <button onClick={getMorePokemons} className={style.button} /* componetizar */ >Show More Pokemons</button> 
        </section>
        
    )
}

export default React.memo(PokemonsContainer);