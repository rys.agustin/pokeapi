// Nav bar 

import React from 'react';
import style from './MainHeader.module.css'
import Navigation from '../UI/Navigation';
const MainHeader = () => {
    return (
        <nav className={style.navigation_container}>
            <img src="https://raw.githubusercontent.com/PokeAPI/media/master/logo/pokeapi_256.png" alt='poke-logo' className={style.navigation_image}></img>
            <div className={style.navigation__items}>
                <Navigation text={'Find your favorite Pokemon'} className={style.navigation__item}/>
                <Navigation text={'Pokedex'} className={style.navigation__item}/>
                <Navigation text={'Berries'} className={style.navigation__item}/>
                <Navigation text={'Games'} className={style.navigation__item}/> 
            </div>
        </nav>
    )
}

export default MainHeader;