import React from "react";
import styles from "./UsersData.module.css";

const UsersData = (props) => {
  return (
    <>
      <ul className={styles.unorderList}>
        {props.usersData.map((userData) => (
          <li
            className={styles.listItem}
            key={userData.id}
          >{`my name is ${userData.name}, my email is ${userData.email}`}</li>
        ))}
      </ul>
    </>
  );
};

export default UsersData;
/*import './App.css';
import UsersData from './UsersData';


const App = () => {
  const usesrData = [{
    name: 'Agus',
    email: 'agus@gmail.com',
    id: Math.random()
  },
  {
    name: 'Martin',
    email: 'martin@gmail.com',
    id: Math.random()
  }
]

  return (
    <>
      <UsersData usersData={usesrData}></UsersData>
    </>
  );
}

export default App; */